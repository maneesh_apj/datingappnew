﻿using API.Data;
using API.DTOs;
using API.Entities;
using API.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using API.Extensions;

namespace API.Controllers
{

    [Authorize]
    public class UsersController : BaseApiController
    {

        private readonly IUserRepository _userRepository;
        private readonly IMapper mapper;
        private readonly IPhotoService _photoService;

        public UsersController(IUserRepository userRepository, IMapper mapper, IPhotoService photoService)
        {
            this._photoService = photoService;
            _userRepository = userRepository;
            this.mapper = mapper;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MembersDto>>> GetUsers()
        {
            var users = await _userRepository.GetUserAsync();
            var userToReturn = mapper.Map<IEnumerable<MembersDto>>(users);
            return Ok(userToReturn);
        }

        [HttpGet("{username}", Name ="username") ]
        public async Task<ActionResult<MembersDto>> GetUser(string username)
        {
            var user = await _userRepository.GetUserByUserNameAsync(username);
            var userToReturn = mapper.Map<MembersDto>(user);
            return Ok(userToReturn);

        }

        [HttpPut]
        public async Task<ActionResult> UpdateUser(MemberUpdateDto memberDto)
        {
            var username = User.GetUsername();
            //User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;
            var user = await _userRepository.GetUserByUserNameAsync(username);
            mapper.Map(memberDto, user);
            _userRepository.Update(user);
            if (await _userRepository.SaveAllAsync()) return NoContent();

            return BadRequest("Failed to update");
        }

        [HttpPost("add-photo")]
        public async Task<ActionResult<PhotoDto>> AddPhoto(IFormFile file)
        {
            var user = await _userRepository.GetUserByUserNameAsync(User.GetUsername());
            
            var result = await _photoService.AddPhotoAsync(file);
            if(result.Error !=null) return BadRequest(result.Error.Message);
            var photo = new Photo
            {
                Url= result.Url.AbsoluteUri,
                PublicId = result.PublicId
            };

            if(user.Photos.Count == 0 )
            {
                photo.IsMain=true;
            }

            user.Photos.Add(photo);
            if(await _userRepository.SaveAllAsync())
            {
                //return mapper.Map<PhotoDto>(photo);    
                return CreatedAtRoute("username", new {username = user.Username}, mapper.Map<PhotoDto>(photo)) ;
            }            

            return BadRequest("Problem addind photo");
        }
        
        [HttpPut("set-main-photo/{photoId}")]        
        public async Task<ActionResult> SetmainPhoto(int photoid)
        {
            var user = await _userRepository.GetUserByUserNameAsync(User.GetUsername());
            Photo photo = user.Photos.FirstOrDefault(x=> x.Id == photoid);
           
            if(photo.IsMain) return BadRequest("This is already your main photo");

            var currentPhotos = user.Photos.FirstOrDefault(x => x.IsMain);
            if(currentPhotos != null) currentPhotos.IsMain = false;
            photo.IsMain = true;

            if(await _userRepository.SaveAllAsync()) return NoContent();
            return BadRequest("Failed to set main photo");
        }
    }
}
