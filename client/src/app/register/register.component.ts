import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};
  constructor(private accountService: AccountService, private toastr: ToastrService, private route: Router) { }
  // @Input() userFromHomeComponent: any;
  @Output() cancelRegister = new EventEmitter();
  ngOnInit(): void {
  }

  register(): any {
    this.accountService.register(this.model).subscribe(response => {
      console.log(response);
      this.toastr.success('Registration successfull');
      this.cancel();
      this.route.navigateByUrl('/matches');
      }, error => {
        this.toastr.error(error.error);
      });
    }


  // tslint:disable-next-line:typedef
  cancel(){
    this.cancelRegister.emit(false);
  }

}
