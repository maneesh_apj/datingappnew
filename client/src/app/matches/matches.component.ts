import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from '../_models/member';
import { MemberServiceService } from '../_services/member-service.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  members$: Observable<Member[]>;
  constructor(private memberService: MemberServiceService) { }

  ngOnInit(): void {
    this.members$ = this.memberService.getMembers();
  }

  // loadMambers(){
  //   console.log(0);
  //   this.memberService.getMembers().subscribe(members => {
  //     this.members= members;
  //   })
  // }
}
