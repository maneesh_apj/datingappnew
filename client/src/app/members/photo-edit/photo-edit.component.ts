import { Component, Input, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { take } from 'rxjs/operators';
import { Member } from 'src/app/_models/member';
import { Photo } from 'src/app/_models/photo';
import { MemberServiceService } from 'src/app/_services/member-service.service';
import { environment } from '../../../environments/environment';
import { User } from '../../_models/user';
import { AccountService } from '../../_services/account.service';

@Component({
  selector: 'app-photo-edit',
  templateUrl: './photo-edit.component.html',
  styleUrls: ['./photo-edit.component.css']
})
export class PhotoEditComponent implements OnInit {
  @Input() member: Member;
  uploader: FileUploader;
  hasBaseDropZoneOve = false;
  baseUrl = environment.apiUrl;
  user: User;
  constructor(private serviceAccount: AccountService, private memberService: MemberServiceService) {
    serviceAccount.currentUser$.pipe(take(1)).subscribe(user => this.user = user);
  }

  ngOnInit(): void {
    this.initializeFileUploader();
  }

  fileOverBase(e: any) {
    this.hasBaseDropZoneOve = e;
  }

  setMainPhoto(photo: Photo){
    this.memberService.setMainPhoto(photo.id).subscribe(() => {
      this.user.photoUrl=photo.url;
      this.serviceAccount.setCurrentUser(this.user);
      this.member.photoUrl=photo.url;
      this.member.photos.forEach(p => {
        if(p.isMain) p.isMain=false;
        if(p.id === photo.id) p.isMain=true;
      })
    });
  }

  initializeFileUploader() {
    this.uploader = new FileUploader({
      url: this.baseUrl + "users/add-photo",
      authToken: "Bearer " + this.user.token,
      isHTML5: true,
      allowedFileType: ["image"],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024
    });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    }

    this.uploader.onSuccessItem = (item, respose, status, headers) => {
      if (respose) {
        const photo = JSON.parse(respose);
        this.member.photos.push(photo);
      }
    }
  }






}
