import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../_models/user';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl ;
  constructor(private http: HttpClient) { }
  private currentUserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentUserSource.asObservable();


  // tslint:disable-next-line:typedef
  login(model: any){
   return this.http.post(this.baseUrl + 'account/login', model).pipe(
      map((response: User) => {
        const user = response;
        if (user) {
          this.setCurrentUser(user);
          //localStorage.setItem('user', JSON.stringify(user));
          //this.currentUserSource.next(user);
        }
        return response;
      })
    );
  }

  // tslint:disable-next-line:typedef
  register(user: User){
    return this.http.post(this.baseUrl + 'account/register', user).pipe(
      map((response: User) => {
        if (response) {
          this.setCurrentUser(response);
       // this.currentUserSource.next(response);
        }
        return response;
      })
    );
  }
  setCurrentUser(user: User): void{
    localStorage.setItem('user', JSON.stringify(user));
      this.currentUserSource.next(user);
   }

  logout(): void {
    localStorage.removeItem('user');
    this.currentUserSource.next(null);
  }

}
