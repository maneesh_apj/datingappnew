import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public registerMode = false;

  constructor(private accoount: AccountService, private route: Router) { }

  ngOnInit(): void {

    this.accoount.currentUser$.subscribe(x=> {
        if(x !=null){
          this.route.navigateByUrl('/matches');
        }
     });

  }

  // tslint:disable-next-line:typedef
  registerToggle(){
     this.registerMode = !this.registerMode;
  }

  cancelRegisterMode(event: boolean): void{
    this.registerMode = event;
  }

}
