import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = { };
  userFullName = '';
  profileImgUrl ='';

  constructor(public accout: AccountService, private router: Router, private alertify: ToastrService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.SetUserName();
   // this.currentUser$ = this.accout.currentUser$;
  }

  SetUserName(): void{
    const usr = localStorage.getItem('user');
    if (usr){
       this.userFullName = JSON.parse(usr).username;
       this.profileImgUrl = JSON.parse(usr).photoUrl;
       // console.log(JSON.parse(usr).username);
    }

  }
  // tslint:disable-next-line:typedef
  login() {
     this.accout.login(this.model).subscribe(next => {
      this.alertify.success('login successfull');
      this.userFullName = next.username;
      this.router.navigateByUrl('/matches');
       });
  }

  loggedIn(): any {

    return true;
  }

  logout(): void {
    this.accout.logout();
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.userFullName = '';
    this.alertify.info('logged out');
    this.router.navigateByUrl('/home');
    // this.router.navigate(['/home']);
  }


}
